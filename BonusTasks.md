# Bonus Task 1

In the proposed scenario, the retries of the requests will result in new transactions, so that the user will buy additional products, that he or she won't intended to do. 

The common solution for this problem is to assign for each transaction its different ID. So that if user tries to make new request, then he should provide **new** transaction ID. If this request **failed**, then retry mechanism on the client-side should repeat request with the **same** transaction ID. On the server side, we should store whether the data for specific transaction ID was commited to the database. So that in case of broken clinet-server connection or other faults, the request with the same transaction ID will not result in more database changes and user will not buy accidentally additional products.

# Bonus Task 2

First of all, I would add a lock for this operation, so that other requests on decreasing balance will not cause race condition. After money transfering is done, then we could release the lock. In case of failure of money transfering, we should revert changes in the database, commit this, and then release the lock. The revert operation could be derived from the request (in particular example we could increase the money in DB by the specified amount), or we could somehow save and then restore previous state.

If the money transfering operation is done in background asynchronously, we could add a callback, which reverts operation in DB in case of failure and releases the lock in both scenarios.

This approach is some kind of implementation of distributed transaction with the compensation/revert logic.

Another approach is called "Eventual Consistency", where only part of the system is consistent. It requires implementing the mechanism for insuring consistency via handling conflicts and their resolution. In the provided particular example we could assume that "real money" value is always "true", and sometimes check the values in DB to be consistent with them.