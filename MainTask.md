# Main task
I decided to check manually the number of products in inventory contraint, so that this is my table:

`CREATE TABLE Inventory (username TEXT, product TEXT, amount INT, UNIQUE(username, product));`

Proof, that everything works:

![](https://i.imgur.com/h4EEytm.png)
_One marshmello was added to Alice BEFORE implementing inventory mechanism, so consider that originally there were 9 marshmellos and Alice had only 90 balance_