import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
invetory_sum = "SELECT sum(amount) as total FROM Inventory WHERE username = %(username)s"
increase_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) on CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="1441",
        host="localhost",
        port=5455
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")
            cur.execute(invetory_sum, obj)
            total = cur.fetchone()[0]
            total = 0 if total is None else total
            if total + amount > 100:
                raise Exception("Too many products in Inventory")
            cur.execute(increase_inventory, obj)
            if cur.rowcount != 1:
                raise Exception("Failed to upsert")

buy_product('Bob', 'marshmello', 1)